#!/bin/ash
#
# artemis-container entrypoint shell script
# Copyright 2019 Abdelhamid MEDDEB <abdelhamid@meddeb.net>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
  
export PATH=$PATH:/usr/share/artemis/bin
BROKER_CONF=/etc/artemis/broker.xml
USERS_CONF=/etc/artemis/artemis-users.properties
ROLES_CONF=/etc/artemis/artemis-users.properties
PARAM_CONF=/etc/artemis/artemis.profile

formatParam() {
  local R=$(echo "$1" | sed "s/\//\\\\\//g")
  R=$(echo "$R" | sed "s/|/\\\|/g")
  R=$(echo "$R" | sed "s/\[/\\\[/g")
  R=$(echo "$R" | sed "s/\]/\\\]/g")
  R=$(echo "$R" | sed "s/\^/\\\^/g")
  R=$(echo "$R" | sed "s/?/\\\?/g")
  R=$(echo "$R" | sed "s/\./\\\./g")
  R=$(echo "$R" | sed "s/+/\\\+/g")
  R=$(echo "$R" | sed 's/\"/\\\"/g')
  R=$(echo "$R" | sed "s/(/\\\(/g")
  R=$(echo "$R" | sed "s/)/\\\)/g")
  R=$(echo "$R" | sed "s/\-/\\\-/g")
  echo "$R"
}

setClusterCredentials() {
  if ls $CONTAINER_CONFIG > /dev/null 2>&1; then
    local RSLT=$(grep "^[ ]*clusteruser" $CONTAINER_CONFIG | awk -F '=' '{print $2}' | sed 's/ //g')
    if [ -n "$RSLT" ]; then			
      sed -i "s/{ARTEMISCLUSTERUSER}/$RSLT/g" $BROKER_CONF
    fi
    RSLT=$(grep "^[ ]*clusterpwd" $CONTAINER_CONFIG | awk -F '=' '{print $2}' | sed 's/ //g')
    if [ -n "$RSLT" ]; then			
      RSLT=$(formatParam $RSLT) 
      sed -i "s/{ARTEMISCLUSTERPWD}/$RSLT/g" $BROKER_CONF
    fi
  fi
}

setTLSConf() {
  if ls $CONTAINER_CONFIG > /dev/null 2>&1; then
    local RSLT=$(grep "^[ ]*kspwd" $CONTAINER_CONFIG | awk -F '=' '{print $2}' | sed 's/ //g')
    if [ -n "$RSLT" ]; then			
      RSLT=$(formatParam $RSLT) 
      sed -i "s/{KSPWD}/$RSLT/g" $BROKER_CONF
    fi
    RSLT=$(grep "^[ ]*ksipwd" $CONTAINER_CONFIG | awk -F '=' '{print $2}' | sed 's/ //g')
    if [ -n "$RSLT" ]; then			
      RSLT=$(formatParam $RSLT) 
      sed -i "s/{KSIPWD}/$RSLT/g" $BROKER_CONF
    fi
    RSLT=$(grep "^[ ]*tspwd" $CONTAINER_CONFIG | awk -F '=' '{print $2}' | sed 's/ //g')
    if [ -n "$RSLT" ]; then			
      RSLT=$(formatParam $RSLT) 
      sed -i "s/{TSPWD}/$RSLT/g" $BROKER_CONF
    fi
    RSLT=$(grep "^[ ]*twowayauth" $CONTAINER_CONFIG | awk -F '=' '{print $2}' | sed 's/ //g')
    RSLT=$(echo "$RSLT" | awk '{print tolower($0)}')
    if [ "$RSLT" == "false" ]; then			
      sed -i "s/needClientAuth=true;//g" $BROKER_CONF
      sed -i "s/verifyHost=true;//g" $BROKER_CONF
    else
      RSLT=$(grep "^[ ]*verifyhost" $CONTAINER_CONFIG | awk -F '=' '{print $2}' | sed 's/ //g')
      RSLT=$(echo "$RSLT" | awk '{print tolower($0)}')
      if [ "$RSLT" == "false" ]; then			
        sed -i "s/verifyHost=true;//g" $BROKER_CONF
      fi
		fi
  fi
}

setJavaConf() {
  if ls $CONTAINER_CONFIG > /dev/null 2>&1; then
    local RSLT=$(grep "^[ ]*minmem" $CONTAINER_CONFIG | awk -F '=' '{print $2}' | sed 's/ //g')
    if [ -n "$RSLT" ]; then			
      sed -i "s/-Xms[0-9a-zA-Z]* /-Xms$RSLT /g" $PARAM_CONF
    fi
    RSLT=$(grep "^[ ]*maxmem" $CONTAINER_CONFIG | awk -F '=' '{print $2}' | sed 's/ //g')
    if [ -n "$RSLT" ]; then			
      sed -i "s/-Xmx[0-9a-zA-Z]* /-Xmx$RSLT /g" $PARAM_CONF
    fi
    RSLT=$(grep "^[ ]*jvmgc" $CONTAINER_CONFIG | awk -F '=' '{print $2}' | sed 's/ //g')
    if [ -n "$RSLT" ]; then			
      sed -i "s/-XX:+Use[0-9a-zA-Z]* /-XX:+Use$RSLT /g" $PARAM_CONF
    fi
    RSLT=$(grep "^[ ]*ipv4" $CONTAINER_CONFIG | awk -F '=' '{print $2}' | sed 's/ //g')
    RSLT=$(echo "$RSLT" | awk '{print tolower($0)}')
    if [ "$RSLT" == "true" ]; then
		  RSLT=$(grep "^JAVA_ARGS" $PARAM_CONF | cut -d '=' -f2-)
      RSLT=$(echo "$RSLT" | sed 's/^"//g')
      RSLT=" -Djava.net.preferIPv4Addresses=true $RSLT"
      sed -i "s/^JAVA_ARGS.*/JAVA_ARGS=\"$RSLT/g" $PARAM_CONF
    fi				
  fi
}

if echo "$@" | grep "artemis" - > /dev/null; then
  if ls $CONFIG_TMPL  > /dev/null 2>&1; then
    rm -f $BROKER_CONF
    cp -p $CONFIG_TMPL $BROKER_CONF
  fi
  sed -i "s/{ARTEMISHOST}/$(hostname -f)/g" $BROKER_CONF
  if ls $USERS_FILE > /dev/null 2>&1; then
	  RSLT=$(diff $USERS_FILE $USERS_CONF)
	  if [ ! -z "$RSLT" }; then
		  cp -p $USERS_FILE $USERS_CONF
    fi
  fi
  if ls $ROLES_FILE > /dev/null 2>&1; then
	  RSLT=$(diff $ROLES_FILE $ROLES_CONF)
	  if [ ! -z "$RSLT" }; then
		  cp -p $ROLES_FILE $ROLES_CONF
    fi
  fi
  if ls $KEYSTORE_FILE > /dev/null 2>&1; then
    RSLT=$(formatParam $KEYSTORE_FILE) 
    sed -i "s/{KSFILE}/$RSLT/g" $BROKER_CONF
  fi
  if ls $KEYSTORE_FILE_INT > /dev/null 2>&1; then
    RSLT=$(formatParam $KEYSTORE_FILE_INT) 
    sed -i "s/{KSIFILE}/$RSLT/g" $BROKER_CONF
  fi
  if ls $TRUSTSTORE_FILE > /dev/null 2>&1; then
    RSLT=$(formatParam $TRUSTSTORE_FILE) 
    sed -i "s/{TSFILE}/$RSLT/g" $BROKER_CONF
  fi
  setClusterCredentials
  setTLSConf
  setJavaConf	
elif [ "$@" == "TEST" ]; then
  exec artemis run 2>&1 >> /var/log/artemis/artemis.out
fi
exec "$@"
