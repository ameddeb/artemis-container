#!/bin/bash
#
# artemis-container image control shell script
# Copyright 2019 Abdelhamid MEDDEB <abdelhamid@meddeb.net>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

VERSION=1.0
CMD=
HELP=

exitError() {
	local MSG=$1
	if [ -z "$MSG" ]; then
		MSG="Fatal error occured."
	fi
	echo "$MSG"
	echo ""
	exit 1
}

checkUser() {
		local USERID=$(id -u)
		if [ $USERID -ne 0 ]; then
				echo "Error, must be run as root user"
				echo ""
				exit 1
		fi
}

checkDocker() {
		RSLT=$(command -v docker)
		if [ -z "$RSLT" ]; then
			exitError "Error, Docker not installed."
		fi
		RSLT=$(ps -ef | grep dockerd | grep -v grep)
		if [ -z "$RSLT" ]; then
				exitError "Error, Docker not running."
		fi
}

showHelp() {
  echo ""
  echo "Usage: $0 start"
  echo "          Start the container"
  echo "       Or"
  echo "       $0 stop"
  echo "          Stop the container"
  echo "       Or"
  echo "       $0 --help|-h"
  echo "          Show this help message"
  echo ""
}		

setParameters() {
	if [ -z "$1" ]; then
		showHelp
		exit 1
	fi
	while [ "$1" != "" ]; do
		case $1 in
			start | stop)
					CMD=$1
					;;
			-h | --help)
					shift
					HELP=$1
					showHelp
					exit 0
					;;
			* )
					showHelp
					exit 1
		esac
		shift
  done
}

createNetwork() {
  local NET=$(docker network ls | grep artemis-net)
  if [ -z "$NET" ]; then
    docker network create --driver bridge artemis-net
  fi
}		

startContainer() {
	echo "Starting artemis container.."
	createNetwork	
  docker run -d --rm --name artemishost --net artemis-net -p 61616:61616 -p 9876:9876/udp \
                --mount src=artemis-data,dst=/var/lib/artemis \
                --mount src=artemis-log,dst=/var/log/artemis \
                ameddeb/artemis-container:$VERSION TEST

	echo "Done."
}

stopContainer() {
	echo "Stopping artemis container.."
	docker stop artemishost
	echo "Done."
}

# main
checkUser
setParameters $@
checkDocker
case $CMD in
	start)
		startContainer
		;;
	stop)
		stopContainer
		;;
esac
