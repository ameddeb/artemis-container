### A Docker container for Apache ActiveMQ Artemis based MoM.

This repository contain source files of **artemis-container** image.

#### Container description 

Message oriented middleware (MoM) based on **Apache ActiveMQ Artemis**. It is built to be production ready with horizontal scalability capability.

- High securised: Built on Alpine Linux. Two-Way TLS authentication enabled.
- High performance messages persistence enabled with asynchronous IO.
- High availability, load balancing and clients failover enabled with cluster wide replicated data storage.

#### Full documentation

See : https://www.meddeb.net/artemis-container

#### Powered by

![alt tag](https://www.meddeb.net/respub/activemq-72.png) - ![alt tag](https://www.meddeb.net/respub/alpinelinux.png)

https://activemq.apache.org/artemis - https://www.alpinelinux.org
