# Apache ActiveMQ Artemis container Docker file
# Copyright 2019 Abdelhamid MEDDEB <abdelhamid@meddeb.net>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
FROM alpine:3.9
LABEL maintainer="Abdelhamid MEDDEB <abdelhamid@meddeb.net>"
ENV CONFIG_TMPL=/broker.xml.tmpl
ENV USERS_FILE=/artemis-users.properties
ENV ROLES_FILE=/artemis-roles.properties
ENV KEYSTORE_FILE=/artemis-ks.p12
ENV KEYSTORE_FILE_INT=/artemis-ksi.p12
ENV TRUSTSTORE_FILE=/artemis-ts.p12
ENV CONTAINER_CONFIG=/container_config
WORKDIR /opt
RUN echo "http://dl-cdn.alpinelinux.org/alpine/v3.9/community" >> /etc/apk/repositories && \
    apk update && \
    apk --no-cache add openjdk8-jre && \
    wget -O /tmp/artemis.tgz 'https://www.apache.org/dyn/closer.cgi?filename=activemq/activemq-artemis/2.6.4/apache-artemis-2.6.4-bin.tar.gz&action=download' && \
    tar --exclude=*/web/* --exclude=*/examples/* -x -v -f /tmp/artemis.tgz && \
    rm -f /tmp/artemis.tgz
RUN apk --no-cache add libaio nss && \	
    mkdir -p /etc/artemis && \
    mkdir -p /var/log/artemis && \
    mkdir -p /tmp/artemis && \
    /opt/apache-artemis-2.6.4/bin/artemis create --silent --name artemis --host artemishost --etc /etc/artemis --data /var/lib/artemis \
                                                 --user artemisuser --password artemispwd --require-login --aio \
                                                 --no-web --no-amqp-acceptor --no-hornetq-acceptor --no-mqtt-acceptor --no-stomp-acceptor \
                                                 --addresses artemis.test /usr/share/artemis && \
    rm -rf /usr/share/artemis/log && \
    rm -rf /usr/share/artemis/tmp && \
    ln -s /var/log/artemis /usr/share/artemis/log && \
    ln -s /tmp/artemis /usr/share/artemis/tmp && \
    addgroup -S -g 661 artemis && adduser -S -u 661 -G artemis artemis
RUN chown artemis:artemis -R /var/lib/artemis && \
    chown artemis:artemis -R /var/log/artemis && \
    chown artemis:artemis -R /etc/artemis && \
    chown artemis:artemis -R /tmp/artemis && \
    mkdir -p /usr/share/artemis/lock && \
    chown artemis:artemis /usr/share/artemis/lock
VOLUME ["/var/lib/artemis"]
VOLUME ["/var/log/artemis"]
EXPOSE 61616
EXPOSE 9876/udp
WORKDIR /usr/share/artemis/bin
COPY entrypoint.sh /usr/share/artemis/bin
RUN chmod 755 /usr/share/artemis/bin/entrypoint.sh
USER artemis
ENTRYPOINT ["./entrypoint.sh"]
CMD ["artemis","run"]
